¿Por qué usar esto?
Tiene una plantilla de firma de correo electrónico HTML para sus usuarios de Mail.app (tal vez algo como esta plantilla de ejemplo). Necesita incorporar la información de contacto del usuario en la plantilla. Este script automatiza ese proceso, solicitando la información del usuario, reemplazando los marcadores de posición de la plantilla con la información del usuario e importando la firma en Mail.app.
Requisitos
Mac OS X 10.7.x - 10.8.x.
El script requiere una conexión a Internet activa, scripts GUI (el script intentará habilitar) y que su plantilla de firma de correo electrónico HTML sea accesible en línea.
Instalación
Es un guión; La instalación no es necesaria. Puede descargar el archivo zip y extraer el script. Al abrir el archivo de script, se abrirá el Editor de AppleScript y podrá ejecutar el script. También puede copiar el texto sin formato de CreateHtmlEmailSignature.scpt y pegarlo en una ventana vacía de AppleScript Editor para guardar el script en su computadora.
Ayuda
La mejor manera de solicitar ayuda es publicar un nuevo problema.
Cuestiones
Informar problemas en la página de seguimiento de incidencias. A continuación se presentan algunos problemas actuales que deben conocer.
iCloud
Después de importar la firma en Mail.app, la firma desaparece. Esto parece estar causado por los documentos de iCloud y las preferencias de sincronización de datos y está siendo trabajado.
Mavericks, Yosemite, El Capitán y Sierra (Mac OS 10.9, 10.10, 10.11 y 10.12)
El guión fue creado usando Lion (10.7) y probado en Mountain Lion (10.8). No tengo un equipo con versiones posteriores para probar o actualizar el script. Si está ejecutando una de esas versiones y desea actualizar la secuencia de comandos, consulte Contribuir.
No se puede establecer elementos de interfaz de usuario activados de la aplicación a true
Este es un problema con Mavericks y versiones posteriores (10.9 +). El proceso para habilitar scripts GUI ha cambiado.
contribuyendo
Contribuciones bienvenidas. Actualmente los probadores de buscadores para Mac OS X versiones 10.9 +. Consulte Contribución para obtener más información.
Licencia
Lea la licencia.
